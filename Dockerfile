FROM php:7.0-apache


# Update sources
RUN apt-get update -y

# Enable apache rewrite
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
# Enable "mod_rewrite" – http://httpd.apache.org/docs/current/mod/mod_rewrite.html
RUN a2enmod rewrite

# Enable "mod_headers" – http://httpd.apache.org/docs/current/mod/mod_headers.html
RUN a2enmod headers

# Enable "mod_expires" – http://httpd.apache.org/docs/current/mod/mod_expires.html
RUN a2enmod expires

#RUN a2dismod mpm_prefork

#RUN a2enmod mpm_event



# Install php extensions
RUN apt-get update && apt-get install -y \
    cron \
    anacron \
    git \
    libpcre3-dev \
    libmcrypt-dev \
    libxml2-dev \
    zlib1g-dev \
    libssh2-1 \
    libssh2-1-dev \
    libpng-dev \
    --no-install-recommends \
    && docker-php-ext-install -j$(nproc) pdo_mysql mbstring soap zip gd \
    && cp /usr/local/bin/php /usr/bin/ \
    && docker-php-ext-install bcmath

RUN apt-get update && apt-get install -y libbz2-dev
RUN docker-php-ext-install bz2

RUN docker-php-ext-configure calendar && docker-php-ext-install calendar

RUN docker-php-ext-install dba

RUN docker-php-ext-install exif

RUN docker-php-ext-install gettext

RUN apt install -y libgmp-dev # idk
RUN docker-php-ext-install gmp # idk

RUN apt-get install -y libc-client-dev libkrb5-dev
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
        && docker-php-ext-install imap \
        && docker-php-ext-enable imap

RUN docker-php-ext-install intl

# RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu
# RUN docker-php-ext-install ldap

RUN \
    apt-get update && \
    apt-get install libldap2-dev -y && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-install ldap

RUN docker-php-ext-install mcrypt

RUN docker-php-ext-install mysqli

RUN docker-php-ext-install pcntl

RUN docker-php-ext-install shmop

RUN docker-php-ext-install sockets

RUN docker-php-ext-install sysvmsg

RUN docker-php-ext-install sysvsem

RUN docker-php-ext-install sysvshm

RUN docker-php-ext-install wddx

# Install PECL extensions
# RUN pecl install xdebug && pecl install mcrypt-1.0.1 && pecl install ssh2-1.1.2
# RUN docker-php-ext-enable xdebug mcrypt ssh2


RUN pecl install redis-5.1.1 \
    && pecl install xdebug-2.8.1 \
    && docker-php-ext-enable redis xdebug

# Install Phalcon
WORKDIR /usr/local/src
RUN git clone https://github.com/phalcon/cphalcon.git --branch v3.1.1 --single-branch
WORKDIR /usr/local/src/cphalcon/build
RUN ./install

WORKDIR /etc/php7/mods-available
RUN echo 'extension=phalcon.so' >> phalcon.ini
RUN docker-php-ext-enable phalcon

WORKDIR /var/www